#!/user/bin/env perl

ensure_path('TEXINPUTS', './biblatex-japanese/latex//;');
$lualatex = 'lualatex %O -kanji=utf8 -no-guess-input-env -synctex=1 -halt-on-error -interaction=nonstopmode -file-line-error %S';
$biber = 'biber --bblencoding=utf8 -u -U --output_safechars';
$max_repeat = 5;
$pdf_mode = 4;
$clean_ext = "nav snm run.xml";
$clean_full_ext = "%R.bbl"
